import 'package:flutter/material.dart';

import 'package:get/get.dart';

import 'package:flutter_challenge_nathaniel_vincent_wibowo/1ST_BUGS/SplashScreen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primarySwatch: Colors.purple, fontFamily: 'ProductSansRegular'),
      home: const SplashScreen(),
    );
  }
}
