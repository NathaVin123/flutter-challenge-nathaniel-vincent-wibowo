// Untuk menyimpelkan dalam pemanggilan gambar
extension StringExtension on String {
  String get png => 'assets/$this.png';
}
