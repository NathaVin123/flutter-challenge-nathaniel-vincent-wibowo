import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_challenge_nathaniel_vincent_wibowo/Utils/extension_image.dart';

import 'Component/Dashboard.dart' as Dashboard;
import 'Component/Akun.dart' as Akun;

class DashboardNavbar extends StatefulWidget {
  DashboardNavbar({Key? key}) : super(key: key);

  @override
  State<DashboardNavbar> createState() => _DashboardNavbarState();
}

class _DashboardNavbarState extends State<DashboardNavbar> {
  int index = 0;

  final screens = [new Dashboard.Dashboard(), new Akun.Akun()];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screens[index],
      bottomNavigationBar: BottomAppBar(
        elevation: 10,
        child: NavigationBarTheme(
          data: NavigationBarThemeData(
              indicatorColor: Colors.black,
              labelTextStyle: MaterialStateProperty.all(TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'OpenSans',
                  color: Colors.black))),
          child: NavigationBar(
            height: 65,
            backgroundColor: Colors.white,
            labelBehavior: NavigationDestinationLabelBehavior.onlyShowSelected,
            selectedIndex: index,
            animationDuration: Duration(seconds: 1),
            onDestinationSelected: (index) =>
                setState(() => this.index = index),
            destinations: [
              NavigationDestination(
                  icon: Icon(
                    Icons.home_rounded,
                    color: Colors.black,
                  ),
                  selectedIcon: Icon(
                    Icons.home_rounded,
                    color: Colors.white,
                  ),
                  label: 'Dashboard'),
              NavigationDestination(
                  icon: Icon(Icons.person, color: Colors.black),
                  selectedIcon: Icon(
                    Icons.person,
                    color: Colors.white,
                  ),
                  label: 'Akun'),
            ],
          ),
        ),
      ),
    );
  }
}
