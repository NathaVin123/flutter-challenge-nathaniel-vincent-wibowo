import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import 'package:flutter_challenge_nathaniel_vincent_wibowo/Utils/extension_image.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  final TextEditingController pass = TextEditingController();
  final TextEditingController confirmPass = TextEditingController();

  bool hidePassword = true;
  bool confirmHidePassword = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: CustomScrollView(
          slivers: [
            SliverFillRemaining(
              hasScrollBody: false,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        'header-login'.png,
                        height: 125,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 30),
                        child: Image.asset(
                          'logo'.png,
                          height: 60,
                        ),
                      ),
                    ],
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: const EdgeInsets.all(25.0),
                      child: Column(
                        // ignore: prefer_const_literals_to_create_immutables
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: Column(
                              children: [
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 8.0),
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      'Sign Up',
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 8.0),
                                  child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                          'Please register your detail to sign in.')),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: Form(
                              key: _formkey,
                              child: Container(
                                child: Column(
                                  children: [
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('Full Name'),
                                    ),
                                    TextFormField(
                                      style: const TextStyle(
                                        fontSize: 12.0,
                                        color: Colors.black,
                                      ),
                                      decoration: InputDecoration(
                                          hintText: 'Full Name',
                                          hintStyle: TextStyle(
                                            fontStyle: FontStyle.italic,
                                            fontSize: 12.0,
                                            color: Colors.grey,
                                          )),
                                      keyboardType: TextInputType.text,
                                      validator: (String? value) {
                                        if (value != null && value.isEmpty) {
                                          return 'Please enter your Full Name';
                                        }
                                        return null;
                                      },
                                    ),
                                    Divider(
                                      height: 25,
                                      color: Colors.white,
                                    ),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('User ID'),
                                    ),
                                    TextFormField(
                                      style: const TextStyle(
                                        fontSize: 12.0,
                                        color: Colors.black,
                                      ),
                                      decoration: InputDecoration(
                                          hintText: 'User ID',
                                          hintStyle: TextStyle(
                                            fontStyle: FontStyle.italic,
                                            fontSize: 12.0,
                                            color: Colors.grey,
                                          )),
                                      keyboardType: TextInputType.text,
                                      validator: (String? value) {
                                        if (value != null && value.isEmpty) {
                                          return 'Please enter your User ID';
                                        }
                                        return null;
                                      },
                                    ),
                                    Divider(
                                      height: 25,
                                      color: Colors.white,
                                    ),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('Password'),
                                    ),
                                    TextFormField(
                                      controller: pass,
                                      style: const TextStyle(
                                        fontSize: 12.0,
                                        color: Colors.black,
                                      ),
                                      decoration: InputDecoration(
                                        hintText: 'Password',
                                        hintStyle: TextStyle(
                                          fontStyle: FontStyle.italic,
                                          fontSize: 12.0,
                                          color: Colors.grey,
                                        ),
                                        suffixIcon: IconButton(
                                          onPressed: () {
                                            setState(() {
                                              hidePassword = !hidePassword;
                                            });
                                          },
                                          color: Colors.black,
                                          icon: Icon(hidePassword
                                              ? Icons.visibility_off
                                              : Icons.visibility),
                                        ),
                                      ),
                                      obscureText: hidePassword,
                                      validator: (String? value) {
                                        if (value != null && value.isEmpty) {
                                          return 'Please enter your Password';
                                        }
                                        return null;
                                      },
                                    ),
                                    Divider(
                                      height: 25,
                                      color: Colors.white,
                                    ),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('Confirm Password'),
                                    ),
                                    TextFormField(
                                      controller: confirmPass,
                                      style: const TextStyle(
                                        fontSize: 12.0,
                                        color: Colors.black,
                                      ),
                                      decoration: InputDecoration(
                                        hintText: ' Confirm Password',
                                        hintStyle: TextStyle(
                                          fontStyle: FontStyle.italic,
                                          fontSize: 12.0,
                                          color: Colors.grey,
                                        ),
                                        suffixIcon: IconButton(
                                          onPressed: () {
                                            setState(() {
                                              confirmHidePassword =
                                                  !confirmHidePassword;
                                            });
                                          },
                                          color: Colors.black,
                                          icon: Icon(confirmHidePassword
                                              ? Icons.visibility_off
                                              : Icons.visibility),
                                        ),
                                      ),
                                      obscureText: confirmHidePassword,
                                      validator: (String? value) {
                                        if (value != null && value.isEmpty) {
                                          return 'Please enter your Confirm Password';
                                        }
                                        if (value != pass.text) {
                                          return 'passwords are not the same';
                                        }
                                        return null;
                                      },
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 8.0),
                                      child: Align(
                                        alignment: Alignment.centerRight,
                                        child: MaterialButton(
                                          shape: StadiumBorder(),
                                          color:
                                              Color.fromARGB(255, 121, 0, 143),
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 25),
                                            child: Text(
                                              'SIGN UP',
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                          onPressed: () {
                                            FocusScope.of(context).unfocus();

                                            if (_formkey.currentState!
                                                .validate()) {
                                              Get.back();
                                              Fluttertoast.showToast(
                                                  msg: 'Register Successfully',
                                                  toastLength:
                                                      Toast.LENGTH_SHORT,
                                                  gravity: ToastGravity.BOTTOM,
                                                  timeInSecForIosWeb: 1,
                                                  backgroundColor: Colors.green,
                                                  textColor: Colors.white,
                                                  fontSize: 14.0);
                                            }
                                          },
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 25.0),
                    child: Center(
                      child: RichText(
                        text: new TextSpan(
                          children: [
                            TextSpan(
                              text: 'Already have an account? ',
                              style: new TextStyle(
                                fontFamily: 'ProductSansRegular',
                                fontSize: 14.0,
                                color: Colors.black,
                              ),
                            ),
                            TextSpan(
                              text: 'Login',
                              style: new TextStyle(
                                fontFamily: 'ProductSansRegular',
                                fontSize: 14.0,
                                color: Colors.orange,
                              ),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  Get.back();
                                },
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
