import 'package:flutter/material.dart';
import 'package:flutter_challenge_nathaniel_vincent_wibowo/1ST_BUGS/LoginScreen.dart';
import 'package:flutter_challenge_nathaniel_vincent_wibowo/Utils/extension_image.dart';
import 'package:flutter_initicon/flutter_initicon.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Akun extends StatefulWidget {
  Akun({Key? key}) : super(key: key);

  @override
  State<Akun> createState() => _AkunState();
}

class _AkunState extends State<Akun> {
  String? userID = "";

  getDataLogin() async {
    SharedPreferences loginData = await SharedPreferences.getInstance();
    setState(() {
      userID = loginData.getString('userID');
    });
  }

  @override
  Widget build(BuildContext context) {
    getDataLogin();
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color.fromARGB(0, 255, 254, 254),
        title: const Text('Akun',
            style: TextStyle(
                color: Colors.black,
                fontSize: 20,
                fontWeight: FontWeight.bold)),
        centerTitle: true,
        actions: [
          Row(
            children: [
              MaterialButton(
                  onPressed: () async {
                    SharedPreferences autoLogin =
                        await SharedPreferences.getInstance();
                    autoLogin.clear();
                    Get.to(LoginScreen());
                  },
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: const Icon(
                          Icons.exit_to_app_rounded,
                          color: Colors.black,
                        ),
                      ),
                      const Text('Logout',
                          style: TextStyle(
                              fontSize: 16,
                              color: Colors.black,
                              fontWeight: FontWeight.bold)),
                    ],
                  )),
            ],
          ),
        ],
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Image.asset(
                'header-login'.png,
              ),
            ),
            Padding(
              padding: EdgeInsets.all(22),
              child: Initicon(
                text: userID,
                backgroundColor: Colors.black,
                size: 80,
              ),
            ),
            Center(
                child: Column(
              children: [
                Text('${userID}',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
              ],
            )),
            Image.asset(
              'footer-splash'.png,
            ),
          ],
        ),
      ),
    );
  }
}
