import 'package:flutter/material.dart';
import 'package:flutter_challenge_nathaniel_vincent_wibowo/1ST_BUGS/LoginScreen.dart';
import 'package:flutter_challenge_nathaniel_vincent_wibowo/Utils/extension_image.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  String? userID = "";

  getDataLogin() async {
    SharedPreferences loginData = await SharedPreferences.getInstance();
    setState(() {
      userID = loginData.getString('userID');
    });
  }

  @override
  Widget build(BuildContext context) {
    getDataLogin();
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color.fromARGB(0, 255, 254, 254),
        title: const Text('Dashboard',
            style: TextStyle(
                color: Colors.black,
                fontSize: 20,
                fontWeight: FontWeight.bold)),
        centerTitle: true,
        actions: [
          Row(
            children: [
              MaterialButton(
                  onPressed: () async {
                    SharedPreferences autoLogin =
                        await SharedPreferences.getInstance();
                    autoLogin.clear();
                    Get.to(LoginScreen());
                  },
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: const Icon(
                          Icons.exit_to_app_rounded,
                          color: Colors.black,
                        ),
                      ),
                      const Text('Logout',
                          style: TextStyle(
                              fontSize: 16,
                              color: Colors.black,
                              fontWeight: FontWeight.bold)),
                    ],
                  )),
            ],
          ),
        ],
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Image.asset(
                'header-login'.png,
              ),
            ),
            Text(
              'Selamat datang, ${userID ?? '-'}',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
            Image.asset(
              'footer-splash'.png,
            ),
          ],
        ),
      ),
    );
  }
}
