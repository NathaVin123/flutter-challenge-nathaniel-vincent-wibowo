import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:flutter_challenge_nathaniel_vincent_wibowo/1ST_BUGS/DashboardNavbar.dart';
import 'package:flutter_challenge_nathaniel_vincent_wibowo/1ST_BUGS/LoginScreen.dart';
import 'package:flutter_challenge_nathaniel_vincent_wibowo/Utils/extension_image.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 2), () => navigateLogin());
  }

  navigateLogin() async {
    SharedPreferences autoLogin = await SharedPreferences.getInstance();

    var loginStatus = autoLogin.getBool('isLogged') ?? false;

    if (loginStatus == true) {
      return Get.off(DashboardNavbar());
    } else {
      return Get.off(LoginScreen());
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return Scaffold(
      body: Container(
        child: CustomScrollView(
          slivers: [
            SliverFillRemaining(
              hasScrollBody: false,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Image.asset(
                    'header-splash'.png,
                  ),
                  Image.asset(
                    'logo'.png,
                  ),
                  Image.asset(
                    'footer-splash'.png,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
