// ignore_for_file: prefer_const_constructors
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:alert_dialog/alert_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:flutter_challenge_nathaniel_vincent_wibowo/1ST_BUGS/DashboardNavbar.dart';
import 'package:flutter_challenge_nathaniel_vincent_wibowo/1ST_BUGS/SignUpScreen.dart';
import 'package:flutter_challenge_nathaniel_vincent_wibowo/Utils/extension_image.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  bool hidePassword = true;

  final TextEditingController userID = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: CustomScrollView(
        slivers: [
          SliverFillRemaining(
            hasScrollBody: false,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Image.asset(
                      'header-login'.png,
                      height: 125,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 30),
                      child: Image.asset(
                        'logo'.png,
                        height: 60,
                      ),
                    ),
                  ],
                ),
                Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.all(25.0),
                    child: Column(
                      // ignore: prefer_const_literals_to_create_immutables
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: Column(
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 8.0),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    'Login',
                                    style: TextStyle(
                                        fontSize: 17,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 8.0),
                                child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text('Please sign in to continue.')),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: Form(
                            key: _formkey,
                            child: Container(
                              child: Column(
                                children: [
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text('User ID'),
                                  ),
                                  TextFormField(
                                    controller: userID,
                                    style: const TextStyle(
                                      fontSize: 12.0,
                                      color: Colors.black,
                                    ),
                                    decoration: InputDecoration(
                                        hintText: 'User ID',
                                        hintStyle: TextStyle(
                                          fontStyle: FontStyle.italic,
                                          fontSize: 12.0,
                                          color: Colors.grey,
                                        )),
                                    keyboardType: TextInputType.text,
                                    onSaved: (String? value) =>
                                        userID.text = value!,
                                    validator: (String? value) {
                                      if (value != null && value.isEmpty) {
                                        alert(
                                          context,
                                          title: Text('Login Failed !'),
                                          content: Text('User ID is empty.'),
                                          textOK: Text('OK'),
                                        );

                                        return 'Please enter your User ID';
                                      }
                                      return null;
                                    },
                                  ),
                                  Divider(
                                    height: 25,
                                    color: Colors.white,
                                  ),
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text('Password'),
                                  ),
                                  TextFormField(
                                    style: const TextStyle(
                                      fontSize: 12.0,
                                      color: Colors.black,
                                    ),
                                    decoration: InputDecoration(
                                      hintText: 'Password',
                                      hintStyle: TextStyle(
                                        fontStyle: FontStyle.italic,
                                        fontSize: 12.0,
                                        color: Colors.grey,
                                      ),
                                      suffixIcon: IconButton(
                                        onPressed: () {
                                          setState(() {
                                            hidePassword = !hidePassword;
                                          });
                                        },
                                        color: Colors.black,
                                        icon: Icon(hidePassword
                                            ? Icons.visibility_off
                                            : Icons.visibility),
                                      ),
                                    ),
                                    obscureText: hidePassword,
                                    validator: (String? value) {
                                      if (value != null && value.isEmpty) {
                                        alert(
                                          context,
                                          title: Text('Login Failed !'),
                                          content: Text('Password is empty.'),
                                          textOK: Text('OK'),
                                        );
                                        return 'Please enter your Password';
                                      }
                                      return null;
                                    },
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8.0),
                                    child: Align(
                                      alignment: Alignment.centerRight,
                                      child: MaterialButton(
                                        shape: StadiumBorder(),
                                        color: Color.fromARGB(255, 121, 0, 143),
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 25),
                                          child: Text(
                                            'LOGIN',
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        onPressed: () async {
                                          FocusScope.of(context).unfocus();

                                          if (_formkey.currentState!
                                              .validate()) {
                                            SharedPreferences loginData =
                                                await SharedPreferences
                                                    .getInstance();
                                            await loginData.setString(
                                                'userID', userID.text);

                                            SharedPreferences autoLogin =
                                                await SharedPreferences
                                                    .getInstance();
                                            autoLogin.setBool("isLogged", true);

                                            Get.offAll(DashboardNavbar());
                                            Fluttertoast.showToast(
                                                msg: 'Login Successfully',
                                                toastLength: Toast.LENGTH_SHORT,
                                                gravity: ToastGravity.BOTTOM,
                                                timeInSecForIosWeb: 1,
                                                backgroundColor: Colors.green,
                                                textColor: Colors.white,
                                                fontSize: 14.0);
                                          }
                                        },
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 25.0),
                  child: Center(
                    child: RichText(
                      text: new TextSpan(
                        children: [
                          TextSpan(
                            text: 'Don\'t have an account? ',
                            style: new TextStyle(
                              fontFamily: 'ProductSansRegular',
                              fontSize: 14.0,
                              color: Colors.black,
                            ),
                          ),
                          TextSpan(
                            text: 'Sign Up',
                            style: new TextStyle(
                              fontFamily: 'ProductSansRegular',
                              fontSize: 14.0,
                              color: Colors.orange,
                            ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                Get.to(SignUpScreen());
                              },
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    ));
  }
}
